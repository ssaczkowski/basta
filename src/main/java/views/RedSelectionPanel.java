package views;

import basta.connection.IPanel;
import basta.connection.netconfiguration.Configuration;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;
import javax.swing.*;
import javax.swing.border.BevelBorder;
import javax.swing.table.DefaultTableModel;

public class RedSelectionPanel implements IPanel {

    private JPanel contentPane;
    private JTable tblRedes;
    private DefaultTableModel modelRedes;
    private String[] columnNames = {"Redes Disponibles"};
    ;
    private String[] columnNames2 = {"Redes Seleccionadas"};
    ;
    private JTable tblRedesAdded;
    private DefaultTableModel modelRedesAdded;
    private JButton btnAddRed;
    private JButton btnRemoveRed;
    private JCheckBox cbxConfig;
    private List<String> redes;
    private List<String> redesSel;

    public RedSelectionPanel(List<String> networks) {

        contentPane = new JPanel();
        contentPane.setBounds(0, 0, 369, 318);
        contentPane.setBackground(Color.LIGHT_GRAY);
        contentPane.setBorder(new BevelBorder(BevelBorder.LOWERED, Color.BLACK, Color.BLACK, Color.BLACK, Color.BLACK));
        contentPane.setLayout(null);

        JLabel lblTitle = new JLabel("Seleccion de Redes");
        lblTitle.setHorizontalAlignment(SwingConstants.CENTER);
        lblTitle.setForeground(Color.BLACK);
        lblTitle.setFont(new Font("Arial", Font.BOLD, 16));
        lblTitle.setBackground(SystemColor.inactiveCaptionBorder);
        lblTitle.setBounds(10, 10, 349, 49);
        getPanel().add(lblTitle);

        JScrollPane scrollPane = new JScrollPane();
        scrollPane.setForeground(Color.BLACK);
        scrollPane.setBackground(Color.LIGHT_GRAY);
        scrollPane.setFocusTraversalKeysEnabled(false);
        scrollPane.setEnabled(false);
        scrollPane.setBounds(10, 84, 130, 197);
        contentPane.add(scrollPane);

        modelRedes = new DefaultTableModel(null, new String[]{"Redes Disponibles"});
        tblRedes = new JTable(modelRedes);
        tblRedes.setSelectionBackground(Color.LIGHT_GRAY);
        tblRedes.setForeground(Color.WHITE);
        tblRedes.setGridColor(Color.WHITE);
        tblRedes.setBackground(Color.BLACK);
        tblRedes.setDefaultEditor(Object.class, null);
        tblRedes.getTableHeader().setReorderingAllowed(false);
        tblRedes.getColumnModel().getColumn(0).setResizable(false);
        scrollPane.setViewportView(tblRedes);

        JSeparator separator = new JSeparator();
        separator.setBounds(10, 71, 349, 2);
        contentPane.add(separator);

        JScrollPane scrollPane_1 = new JScrollPane();
        scrollPane_1.setForeground(Color.BLACK);
        scrollPane_1.setFocusTraversalKeysEnabled(false);
        scrollPane_1.setEnabled(false);
        scrollPane_1.setBackground(Color.LIGHT_GRAY);
        scrollPane_1.setBounds(229, 84, 130, 197);
        contentPane.add(scrollPane_1);

        modelRedesAdded = new DefaultTableModel(null, new String[]{"Redes Seleccionadas"});
        tblRedesAdded = new JTable(modelRedesAdded);
        tblRedesAdded.setSelectionBackground(Color.LIGHT_GRAY);
        tblRedesAdded.setForeground(Color.WHITE);
        tblRedesAdded.setGridColor(Color.WHITE);
        tblRedesAdded.setBackground(Color.BLACK);
        tblRedesAdded.setDefaultEditor(Object.class, null);
        tblRedesAdded.getTableHeader().setReorderingAllowed(false);
        tblRedesAdded.getColumnModel().getColumn(0).setResizable(false);

        scrollPane_1.setViewportView(tblRedesAdded);

        btnAddRed = new JButton(">>");
        btnAddRed.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent arg0) {
                int filaSeleccion = getTblRedes().getSelectedRow();

                if (filaSeleccion >= 0) {
                    redesSel.add(redes.get(filaSeleccion));
                    redes.remove(filaSeleccion);
                    update();

                } else {
                    JOptionPane.showMessageDialog(null, "Seleccione una fila");
                }
            }
        });
        btnAddRed.setBounds(150, 154, 69, 23);
        contentPane.add(btnAddRed);

        btnRemoveRed = new JButton("<<");
        btnRemoveRed.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                int filaSeleccion = getTblRedesAdded().getSelectedRow();

                if (filaSeleccion >= 0) {
                    redes.add(redesSel.get(filaSeleccion));
                    redesSel.remove(filaSeleccion);
                    update();

                } else {
                    JOptionPane.showMessageDialog(null, "Seleccione una fila");
                }
            }
        });
        btnRemoveRed.setBounds(150, 188, 69, 23);
        contentPane.add(btnRemoveRed);

        cbxConfig = new JCheckBox("Ver ventana de configuraci\u00F3n de redes ya configuradas");
        cbxConfig.setBounds(10, 288, 349, 23);
        contentPane.add(cbxConfig);

        this.redes = networks;
        this.redesSel = new ArrayList<>();
        this.update();
    }

    public JTable getTblRedes() {
        return tblRedes;
    }

    public DefaultTableModel getModelRedes() {
        return modelRedes;
    }

    public String[] getColumnNames() {
        return columnNames;
    }

    public JTable getTblRedesAdded() {
        return tblRedesAdded;
    }

    public DefaultTableModel getModelRedesAdded() {
        return modelRedesAdded;
    }

    public String[] getColumnNames2() {
        return columnNames2;
    }

    public List<String> getRedesSel() {
        return this.redesSel;
    }

    public JCheckBox getCbxConfig() {
        return cbxConfig;
    }

    public void update() {

        getModelRedes().setRowCount(0);
        getModelRedes().setColumnCount(0);
        getModelRedes().setColumnIdentifiers(getColumnNames());

        for (int i = 0; i < redes.size(); i++) {
            Object[] fila = {redes.get(i)};
            getModelRedes().addRow(fila);
        }

        getModelRedesAdded().setRowCount(0);
        getModelRedesAdded().setColumnCount(0);
        getModelRedesAdded().setColumnIdentifiers(getColumnNames2());

        for (int i = 0; i < redesSel.size(); i++) {
            Object[] fila = {redesSel.get(i)};
            getModelRedesAdded().addRow(fila);
        }
    }

    @Override
    public JPanel getPanel() {
        return this.contentPane;
    }

    @Override
    public Configuration getConfiguration() {
        return null;
    }

    @Override
    public void setConfiguration(Configuration config) {

    }

    @Override
    public String getName() {
        return "RedSelectionPanel";
    }
}