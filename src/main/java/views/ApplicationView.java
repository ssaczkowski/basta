package views;

import java.awt.Color;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import javax.swing.border.BevelBorder;

public class ApplicationView extends JFrame {

    private static final long serialVersionUID = 1L;

    private JPanel panel;
    private JButton btnCancel;
    private JButton btnNext;
    private JButton btnPrev;

    public ApplicationView() {

        setAlwaysOnTop(false);
        setResizable(false);

        setTitle("BASTA");
        setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        setBounds(100, 100, 400, 415);
        setLocationRelativeTo(null);
        JPanel staticPane = new JPanel();
        staticPane.setBackground(Color.LIGHT_GRAY);
        staticPane.setBorder(new EmptyBorder(5, 5, 5, 5));
        setContentPane(staticPane);
        staticPane.setLayout(null);

        panel = new JPanel();
        panel.setBorder(new BevelBorder(BevelBorder.LOWERED, Color.BLACK, Color.BLACK, Color.BLACK, Color.BLACK));
        panel.setBounds(10, 11, 369, 318);
        staticPane.add(panel);
        panel.setLayout(null);

        JSeparator separator_1 = new JSeparator();
        separator_1.setBounds(10, 340, 369, 2);
        staticPane.add(separator_1);

        btnCancel = new JButton("Cancelar");
        btnCancel.setBounds(262, 353, 117, 23);
        getContentPane().add(btnCancel);

        btnNext = new JButton("Siguiente");
        btnNext.setBounds(136, 353, 116, 23);
        staticPane.add(btnNext);

        btnPrev = new JButton("Atras");
        btnPrev.setBounds(10, 353, 116, 23);
        btnPrev.setEnabled(false);
        staticPane.add(btnPrev);
    }

    public JButton getBtnCancelar() {
        return btnCancel;
    }

    public JButton getBtnNext() {
        return btnNext;
    }

    public void setMovablePane(JPanel movablePane) {
        panel.removeAll();
        panel.add(movablePane);
        panel.repaint();
    }

    public JButton getBtnPrev() {
        return btnPrev;
    }

    public void setBtnPrev(JButton btnPrev) {
        this.btnPrev = btnPrev;
    }
}