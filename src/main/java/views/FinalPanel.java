package views;

import basta.connection.IPanel;
import basta.connection.netconfiguration.Configuration;

import java.awt.*;
import javax.swing.*;
import javax.swing.border.BevelBorder;

public class FinalPanel implements IPanel {

    private JPanel contentPane;

    public FinalPanel()
    {
        contentPane = new JPanel();
        contentPane.setBounds(0, 0, 369, 318);
        contentPane.setBackground(Color.LIGHT_GRAY);
        contentPane.setBorder(new BevelBorder(BevelBorder.LOWERED, Color.BLACK, Color.BLACK, Color.BLACK, Color.BLACK));
        contentPane.setLayout(null);

        JLabel lblTitle = new JLabel("¡Todo Listo!");
        lblTitle.setHorizontalAlignment(SwingConstants.CENTER);
        lblTitle.setForeground(Color.BLACK);
        lblTitle.setFont(new Font("Arial", Font.BOLD, 16));
        lblTitle.setBackground(SystemColor.inactiveCaptionBorder);
        lblTitle.setBounds(10, 10, 349, 49);
        getPanel().add(lblTitle);

        JSeparator separator = new JSeparator();
        separator.setBounds(10, 71, 349, 2);
        contentPane.add(separator);

        JTextArea textArea = new JTextArea();
        textArea.setFont(new Font("Tahoma", Font.BOLD, 14));
        textArea.setEditable(false);
        textArea.setBackground(Color.LIGHT_GRAY);
        textArea.setText(
                "\tTodas las redes sociales selecionadas\r\nfueron configuradas.\r\nSi desea confirmar presione \"Confirmar\".");
        textArea.setBounds(10, 79, 349, 229);
        contentPane.add(textArea);
    }

    @Override
    public JPanel getPanel() {
        return this.contentPane;
    }

    @Override
    public Configuration getConfiguration() {
        return null;
    }

    @Override
    public void setConfiguration(Configuration config) {

    }

    @Override
    public String getName() {
        return "FinalPanel";
    }
}