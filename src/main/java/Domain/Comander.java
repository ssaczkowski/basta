package Domain;

import controllers.ApplicationController;

import java.util.HashMap;
import java.util.Map;

public class Comander implements IComand {

    public static Map<String, IComand> comands;

    public Comander() {
        comands = new HashMap<>();
        comands.put("Hello", new HelloComand());
        comands.put("RedSelection", new RedSelectionComand());
        comands.put("Configuration", new ConfigurationComand());
        comands.put("Final", new FinalComand());
    }

    @Override
    public void nextStep(ApplicationController appc) {
        if (appc.getCurrentPane() == null) {
            comands.get("Hello").nextStep(appc);
        } else if (appc.getCurrentPane().getName().equals("HelloPanel")) {
            comands.get("RedSelection").nextStep(appc);
        } else if (appc.getCurrentPane().getName().equals("FinalPanel")) {
            comands.get("Final").nextStep(appc);
        } else {
            comands.get("Configuration").nextStep(appc);
        }
    }

    @Override
    public void prevStep(ApplicationController appc) {
        if (appc.getCurrentPane().getName().equals("RedSelectionPanel")) {
            comands.get("RedSelection").prevStep(appc);
        } else if (appc.getCurrentPane().getName().equals("FinalPanel")) {
            comands.get("Final").prevStep(appc);
        } else {
            comands.get("Configuration").prevStep(appc);
        }
    }
}