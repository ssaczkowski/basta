package Domain;

import basta.connection.IPanel;
import basta.connection.NetRegistry;
import basta.connection.PanelRegistry;
import basta.connection.netconfiguration.Configuration;

import java.util.ArrayList;
import java.util.List;


public class Context {

    public List<String> getRedes() {

        return new ArrayList(NetRegistry.getInstance().keySet());
    }

    public Configuration getConfig(String network) {
        return NetRegistry.getInstance().get(network).getConfiguration();
    }

    public void setConfiguration(String network, Configuration config) {
        NetRegistry.getInstance().get(network).setConfiguration(config);
    }

    public IPanel getPanel(String network) {
        return PanelRegistry.getInstance().get(network);
    }
}
