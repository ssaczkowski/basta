package Domain;

import controllers.ApplicationController;
import views.HelloPanel;

public class HelloComand implements IComand {
    @Override
    public void nextStep(ApplicationController appc) {
        appc.setCurrentPane(new HelloPanel());
    }

    public void prevStep(ApplicationController appc) {
    }
}
