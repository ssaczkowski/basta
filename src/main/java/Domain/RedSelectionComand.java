package Domain;

import controllers.ApplicationController;
import views.HelloPanel;
import views.RedSelectionPanel;

public class RedSelectionComand implements IComand {
    @Override
    public void nextStep(ApplicationController appc) {
        appc.setCurrentPane(new RedSelectionPanel(appc.getContext().getRedes()));
        appc.getView().getBtnPrev().setEnabled(true);
    }

    public void prevStep(ApplicationController appc) {
        appc.setCurrentPane(new HelloPanel());
        appc.getView().getBtnPrev().setEnabled(false);
    }
}
