package Domain;

import basta.connection.netconfiguration.Configuration;
import controllers.ApplicationController;
import views.FinalPanel;

import java.util.Iterator;
import java.util.Map;

public class FinalComand implements IComand {

    private boolean aux;

    public FinalComand() {
        this.aux = false;
    }

    @Override
    public void nextStep(ApplicationController appc) {
        if (aux) {
            Map<String, Configuration> mp = appc.getConfigs();
            Iterator it = mp.entrySet().iterator();
            while (it.hasNext()) {
                Map.Entry pair = (Map.Entry) it.next();
                appc.getContext().setConfiguration((String) pair.getKey(), (Configuration) pair.getValue());
                it.remove();
            }
        } else {
            aux = true;
            appc.setCurrentPane(new FinalPanel());
            appc.getView().getBtnNext().setText("Confirmar");
        }

        //TODO: Ver como se sigue despues de enviar las configuraciones.
    }

    public void prevStep(ApplicationController appc) {
        aux = false;
        appc.getView().getBtnNext().setText("siguiente");
        Comander.comands.get("Configuration").prevStep(appc);
    }
}
