package Domain;

import controllers.ApplicationController;

public interface IComand {

    public void nextStep(ApplicationController appc);

    public void prevStep(ApplicationController appc);
}
