package Domain;

import basta.inicialization.BastaModel;
import controllers.ApplicationController;

public class MainClass {
    public static void main(String[] args){

        BastaModel.getInstance(true).init();

        Context context = new Context();
        ApplicationController appC = new ApplicationController(context);
    }
}