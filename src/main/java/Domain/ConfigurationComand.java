package Domain;

import controllers.ApplicationController;
import views.RedSelectionPanel;


public class ConfigurationComand implements IComand {

    @Override
    public void nextStep(ApplicationController appc) {

        if (appc.getCurrentPane().getName().equals("RedSelectionPanel")) {
            RedSelectionPanel redS = (RedSelectionPanel) appc.getCurrentPane();
            appc.setReconfigure(redS.getCbxConfig().isSelected());
            appc.setRedesSel(redS.getRedesSel());
        } else {
            appc.getConfigs().put(appc.getCurrentPane().getName(), appc.getCurrentPane().getConfiguration());
        }

        boolean aux = true;
        while (aux) {
            if (appc.getStep() < appc.getRedesSel().size()) {
                if (appc.getReconfigure() || appc.getContext().getConfig(appc.getRedesSel().get(appc.getStep())) == null) {
                    appc.setCurrentPane(appc.getContext().getPanel(appc.getRedesSel().get(appc.getStep())));
                    appc.getCurrentPane().setConfiguration(appc.getContext().getConfig(appc.getRedesSel().get(appc.getStep())));
                    aux = false;
                }
                appc.setStep(appc.getStep() + 1);
            } else {
                Comander.comands.get("Final").nextStep(appc);
                aux = false;
            }
        }
    }

    public void prevStep(ApplicationController appc) {
        boolean aux = true;
        while (aux) {
            if (appc.getStep() > 0) {
                appc.setStep(appc.getStep() - 1);
                if (appc.getReconfigure() || appc.getContext().getConfig(appc.getRedesSel().get(appc.getStep())) == null) {
                    appc.setCurrentPane(appc.getContext().getPanel(appc.getRedesSel().get(appc.getStep())));
                    appc.getCurrentPane().setConfiguration(appc.getConfigs().get(appc.getRedesSel().get(appc.getStep())));

                    aux = false;
                }
            } else {
                appc.setCurrentPane(new RedSelectionPanel(appc.getContext().getRedes()));
                aux = false;
            }
        }
    }
}