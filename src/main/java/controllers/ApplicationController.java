package controllers;

import Domain.Comander;
import Domain.Context;
import Domain.IComand;
import basta.connection.IPanel;
import basta.connection.netconfiguration.Configuration;
import views.ApplicationView;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ApplicationController implements ActionListener {

    private ApplicationView view;
    private Context context;
    private IPanel currentPane;
    private boolean reconfigure;
    private List<String> redesSel;
    private int step;
    private IComand comander;
    private Map<String, Configuration> configs;

    public ApplicationController(Context context) {
        this.context = context;
        this.comander = new Comander();
        this.view = new ApplicationView();
        this.step = 0;
        this.configs = new HashMap<>();
        this.view.getBtnCancelar().addActionListener(this);
        this.view.getBtnNext().addActionListener(this);
        this.view.getBtnPrev().addActionListener(this);
        initialize();
    }

    public void initialize() {
        this.view.setVisible(true);
        this.nextStep();
        this.updatePane(currentPane.getPanel());
    }

    public void updatePane(JPanel pane) {
        this.view.setMovablePane(pane);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getSource() == this.view.getBtnNext()) {
            this.nextStep();
        } else if (e.getSource() == this.view.getBtnPrev()) {
            this.prevStep();
        } else if (e.getSource() == this.view.getBtnCancelar()) {
            this.view.dispose();
        }
    }

    public void nextStep() {
        comander.nextStep(this);
        this.updatePane(this.currentPane.getPanel());
    }

    public void prevStep() {
        comander.prevStep(this);
        this.updatePane(this.currentPane.getPanel());
    }

    public void setCurrentPane(IPanel pane) {
        this.currentPane = pane;
    }

    public IPanel getCurrentPane() {
        return this.currentPane;
    }

    public void setReconfigure(boolean rec) {
        this.reconfigure = rec;
    }

    public boolean getReconfigure() {
        return this.reconfigure;
    }

    public void setStep(int step) {
        this.step = step;
    }

    public int getStep() {
        return this.step;
    }

    public void setRedesSel(List<String> redes) {
        this.redesSel = redes;
    }

    public List<String> getRedesSel() {
        return this.redesSel;
    }

    public Context getContext() {
        return this.context;
    }

    public ApplicationView getView() {
        return this.view;
    }

    public Map<String, Configuration> getConfigs() {
        return configs;
    }
}